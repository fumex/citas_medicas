<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login','AuthController@login');
Route::post('create-user','UserController@store');
Route::get('dni','AuthController@DNI');
Route::get('ruc','AuthController@ruc');
Route::group(['middleware'=>'auth.jwt'], function (){
    //user
    Route::get('logout','AuthController@logout');
    Route::get('me','AuthController@me');
    Route::post('update-user/{dni}','UserController@update');
    Route::get('list-users','UserController@index');
    Route::get('user/{dni}','UserController@show');
    Route::get('destroy-user/{dni}','UserController@destroy');
    Route::get('active-user/{dni}','UserController@active');

    //typeUser
    Route::get('list-type-users','TypeUserController@index');
    Route::get('type-user/{dni}','TypeUserController@show');
});
