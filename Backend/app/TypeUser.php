<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeUser extends Model
{
    /**
     * @var string
     */
    protected $table='type_users';
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
   /* public function Users()
    {
        return $this->hasOne(User::class);
    }*/
}
