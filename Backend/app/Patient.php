<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
     * @var string
     */
    protected $table = 'patient';
    /**
     * @var array
     */
    protected  $guarded = [];

}
