<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    /**
     * @var string
     */
    protected $table = 'doctor';
    /**
     * @var array
     */
    protected $guarded = [];

}
