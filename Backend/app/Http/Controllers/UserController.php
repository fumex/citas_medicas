<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegistrationFormRequest;
use App\User;


class UserController extends Controller
{
    /**
     * @var bool
     */
    public $loginAfterSignUp =  true;

    public $auth;

    /**
     * UserController constructor.
     * @param AuthController $_auth
     */
    public function __construct( AuthController $_auth)
    {
        $this->auth = $_auth;
    }

    /**
     * @param RegistrationFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function store(RegistrationFormRequest $request){
        $user = new User();
        $user->dni = $request->dni;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->birthday = $request->birthday;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->user_create = 'admin';
        $user->user_update = null;
        $user->save();
        if($this->loginAfterSignUp){
            return $this->auth->login($request);
        }
        return response()->json([
            'success'=> true,
            'data' => $user
        ],200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $users = User::where("state","=",true)->get();
        return response()->json([
            'success'=>true,
            'data'=>$users
        ],200);
    }

    /**
     * @param $dni
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($dni){
        $user =User::where("dni","=",$dni)->get();
        if(!$user){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry. User with dni'.$dni.'cannot be found.'
            ],400);
        }
        return response()->json([
            'success'=>true,
            'data'=>$user
        ],200);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id,Request $request){
        $user = User::find($id);
        if(!$user){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry. User cannot be found.',
            ],400);
        }
        $user->dni = $request->dni;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->birthday = $request->birthday;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->user_create = 'admin';
        $user->user_update = $this->auth->me()->getData()->data->dni;

        $update = $user->save();
        if ($update) {
            return response()->json([
                'success' => true,
                'data'=>$user,
            ],200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task could not be updated.'
            ], 500);
        }
    }

    public function destroy($id){
        $user = User::find($id);
        if(!$user){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry. User cannot be found.',
            ],400);
        }
        $userDestroy=$this->auth->me()->getData()->data->dni;
        if($user->dni == $userDestroy){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry. Cannot delete itself',
            ],400);
        }
        $user->state=false;
        $user->user_update = $userDestroy;
        $update = $user->save();
        if ($update) {
            return response()->json([
                'success' => true,
                'data'=>$user,
            ],200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task could not be updated.'
            ], 500);
        }
    }
    public function active($id){
        $user = User::find($id);
        if(!$user){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry. User cannot be found.',
            ],400);
        }
        $userDestroy=$this->auth->me()->getData()->data->dni;
        if($user->dni == $userDestroy){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry. Cannot delete itself',
            ],400);
        }
        $user->state=true;
        $user->user_update = $userDestroy;
        $update = $user->save();
        if ($update) {
            return response()->json([
                'success' => true,
                'data'=>$user,
            ],200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task could not be updated.'
            ], 500);
        }
    }
}
