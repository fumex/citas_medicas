<?php

namespace App\Http\Controllers;


use App\TypeUser;
use Illuminate\Http\Request;

class TypeUserController extends Controller
{
    public $auth;

    public function __construct(AuthController $_auth)
    {
        $this->auth = $_auth;
    }

    public function index(){
        $typeUsers = TypeUser::where("state","=",true)->get();
        return response()->json([
            'success'=>true,
            'data'=>$typeUsers
        ],200);
    }

    public function show($dni){
        $typeUser = TypeUser::where("dni_user","=",$dni)->get();
        if(!$typeUser){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry. User with dni'.$dni.'cannot be found.'
            ],400);
        }
        return response()->json([
            'success'=>true,
            'data'=>$typeUser
        ],200);
    }

    public function store(Request $request){
        $typeUser = new TypeUser();
    }

    public function update($id, Request $request){

    }
    public function destroy($id){

    }
    public function active($id){

    }

}
