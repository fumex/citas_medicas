<?php

namespace App\Http\Controllers;



use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

use Peru\Http\ContextClient;
use Peru\Jne\{Dni, DniParser};

use Peru\Sunat\{HtmlParser, Ruc, RucParser};


class AuthController extends Controller
{
    public $loginAfterSignUp = true;

    private $jwtAuth;

    /**
     * AuthController constructor.
     * @param JWTAuth $jwtAuth
     */
    public function __construct(JWTAuth $jwtAuth)
    {
        $this->jwtAuth = $jwtAuth;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function login(Request $request){
        $input = $request->only('email','password');
        $token = null;
        if(!$token = JWTAuth::attempt($input)){
            return response()->json([
                'success'=>false,
                'message'=>'invalid credentials'
            ],401);
        }
        return response()->json([
            'success'=>true,
            'token'=>$token
        ],200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(){
        $token_last = $this->jwtAuth->getToken();
        $token = $this->jwtAuth->refresh($token_last);

        return response()->json([
            'success'=>true,
            'token'=>$token
        ],200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(){
        $token = JWTAuth::getToken();
        try {
            JWTAuth::invalidate($token);
            return response()->json([
                'success'=>true,
                'message'=>'User logged out successfully'
            ],200);
        }catch (JWTException $exception){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry, the user cannot be logged out'
            ],500);
        }
    }

    public function me(){
        try {
            if(!$user= JWTAuth::parseToken()->authenticate()){
                return response()->json(['error'=>'user not found'],404);
            }
        }catch (JWTException $exception){
            return response()->json([
                'success'=>false,
                'message'=>'Sorry, the user cannot find'
            ],500);
        }

        return response()->json([
            'success'=>true,
            'data'=>$user
        ],200);
    }


    public function DNI(){

        $dni ='46943074';
        $sc = new Dni(new ContextClient(), new DniParser());
        $person = $sc->get($dni);

        if(!$person){
            return response()->json([
                'success'=>false,
                'message'=>' no encontrado'
            ],500);

        }
        return response()->json([
            'success'=>true,
            'data'=>$person
        ],200);
    }

    public function ruc(){
        $ruc = '20400581925';
        $cs =  new Ruc(new ContextClient(), new RucParser(new HtmlParser()));
        $company = $cs->get($ruc);
        if(!$company){
            return response()->json([
                'success'=>false,
                'message'=>' no encontrado'
            ],500);

        }
        return response()->json([
            'success'=>true,
            'data'=>$company
        ],200);
    }
}
