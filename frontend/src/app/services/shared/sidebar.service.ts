import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  menu: any = [
    {
      title: 'Persona',
      icon: 'mdi mdi-account-settings-variant',
      submenu: [
        { title: 'Dashboard', url: '/dashboard' },
      ]
    }
  ];
  constructor() { }
}
