import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { User } from '../login-model';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn:'root'})
export class AuthenticationService {

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http:HttpClient,private router:Router){
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue():User{
        return  this.currentUserSubject.value;
    }

    login(dni:string, password:string){
        return  this.http.post<any>(`${environment.apiUrl}/api/login`,{dni,password})
            .pipe(map(user=>{
                //iniciar sesión correctamente si hay un token jwt en la respuesta
                if(user && user.token){
                    //almacenar detalles de usuario y token jwt en almacenamiento local
                    // para mantener al usuario conectado entre actualizaciones de página
                    localStorage.setItem('currentUser',JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }
                return user;
            }));
    }

    logout(){
      return   this.http.get(`${environment.apiUrl}/api/logout`).pipe(map(user=>{
            localStorage.removeItem('currentUser');
            this.currentUserSubject.next(null);
            this.router.navigate(['auth/login']);
        }));
         
    }

}