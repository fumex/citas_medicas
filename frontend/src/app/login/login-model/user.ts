import { Role } from "./role";

export class User{
    id: number;
    dni :string;
    name :string;
    email:string;
    password:string;
    role:Role;
    token?:string;
}