import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

export class Parent {
    public showProcessing: boolean = false;
    public form: FormGroup;
    public operationNew = 'NUEVO';
    public operationDelete = 'ELIMINAR';
    public operationUpdate = 'EDITAR';
    public operationView = 'VER';
    public disabledEdit: boolean = true;
    public title: string;
    public actionView: string;
    public actionEdit: string;

    getDateOfStringYMD(strFechaYMD: string): Date {
        const array = strFechaYMD.split('-');
        return new Date(parseInt(array[0]), parseInt(array[1]) - 1, parseInt(array[2]));
    }

    getDateNowYMD(): string {
        const today = new Date();
        let dd: string = String(today.getDate());
        let mm: string = String(today.getMonth() + 1); // January is 0!
        const yyyy: string = String(today.getFullYear());
        if (dd.length === 1) {
            dd = '0' + dd;
        }
        if (mm.length === 1) {
            mm = '0' + mm;
        }
        return yyyy + '-' + mm + '-' + dd;
    }

    getDateNowDMY(): string {
        const today = new Date();
        let dd: string = String(today.getDate());
        let mm: string = String(today.getMonth() + 1); // January is 0!
        const yyyy: string = String(today.getFullYear());
        if (dd.length === 1) {
            dd = '0' + dd;
        }
        if (mm.length === 1) {
            mm = '0' + mm;
        }
        return mm + '-' + dd + '-' + yyyy;
    }
}
