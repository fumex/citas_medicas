import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { CoreModule } from '../core/core.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AngularMaterialModule } from '../core/angular-material.module';
import { AccountSettingsComponent } from './account-settings/account-settings.component';



@NgModule({
  declarations: [PagesComponent, DashboardComponent, AccountSettingsComponent],
  imports: [
    AngularMaterialModule,
    CommonModule,
    CoreModule,
    FormsModule,
    PagesRoutingModule,
    ReactiveFormsModule
  ],
  exports: [
    DashboardComponent,
    PagesComponent
  ]
})
export class PagesModule { }
