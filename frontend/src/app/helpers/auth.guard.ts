import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Router, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../login/login-service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router:Router,
    private authenticationService:AuthenticationService

  ){}

  canActivate( router: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    const currentUser = this.authenticationService.currentUserValue;
    if(currentUser){
      //Comprobar si la ruta esta restringida por rol
      if( router.data.roles && router.data.roles.indexOf(currentUser.role)===-1){
          this.router.navigate(['/']);
          return false;
      }
      // si a iniciado seccion devuelve true
      return true;
    }
    this.router.navigate(['/login'],{queryParams:{returnUrl:state.url}});
    return false; 
  }
 
}
