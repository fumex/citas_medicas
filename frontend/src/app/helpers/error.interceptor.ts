import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http'
import { AuthenticationService } from '../login/login-service';
import { catchError } from 'rxjs/operators';
@Injectable()
export class  ErrorInterceptor implements HttpInterceptor{
  constructor(private authenticationService:AuthenticationService){}

   intercept(request: HttpRequest<any>, next:HttpHandler):Observable<HttpEvent<any>>{
        return next.handle(request).pipe(catchError(err =>{
            if([401, 403].indexOf(err.status)!==-1){
                //Cierre automatico si la respuesta del api es no autorizada o 403 no autorizado
                this.authenticationService.logout();
                location.reload(true);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }

}
