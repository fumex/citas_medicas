import { environment } from './../../environments/environment.prod';
import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../login/login-service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor{
    constructor(private authenticateService: AuthenticationService){}

    intercept(request: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>>{
        //agrega el encabezado de autenticación con jwt si el usuario ha iniciado sesión y
        // la solicitud es a la URL de la API
        const currentUser = this.authenticateService.currentUserValue;
        const isLoggedIn = currentUser && currentUser.token;
        const isApiUrl = request.url.startsWith(environment.apiUrl);
        if(isLoggedIn && isApiUrl){
            request = request.clone({
                setHeaders:{
                    Authorization: `Bearer ${currentUser.token}`
                }
            });
        }

        return next.handle(request)
    }
}